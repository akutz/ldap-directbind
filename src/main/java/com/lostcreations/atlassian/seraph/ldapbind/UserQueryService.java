package com.lostcreations.atlassian.seraph.ldapbind;

import static com.google.common.base.Preconditions.*;
import static com.lostcreations.atlassian.seraph.ldapbind.PropertiesService.*;
import java.security.Principal;

/**
 * A service for querying the local users.
 * 
 * @author akutz
 * 
 */
public class UserQueryService
{
    private final Object stashUserService;

    public UserQueryService(Object stashUserService)
    {
        this.stashUserService = stashUserService;
    }

    /**
     * Gets a user principal from the user's name if the user exists, otherwise
     * a null reference is returned.
     * 
     * @param userName A user name.
     * @return The user's principal if the user exists; otherwise null.
     */
    public Principal getUserByName(final String userName)
    {
        checkNotNull(userName);

        if (IS_JIRA)
        {
            return com.atlassian.jira.component.ComponentAccessor
                .getUserManager()
                .getUserByName(userName);
        }
        else if (IS_CONFLUENCE)
        {
            return ((com.atlassian.confluence.user.UserAccessor) com.atlassian.spring.container.ContainerManager
                .getComponent("userAccessor")).getUser(userName);
        }
        else if (IS_STASH)
        {
            return ((com.atlassian.stash.user.UserService) this.stashUserService)
                .getUserByName(userName);
        }
        else
        {
            return new Principal()
            {
                @Override
                public String getName()
                {
                    return userName;
                }
            };
        }
    }
}
