package com.lostcreations.atlassian.seraph.ldapbind;

import static com.google.common.base.Preconditions.*;
import java.util.Hashtable;
import java.util.Properties;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import com.google.common.base.Strings;

/**
 * A service for authenticating against LDAP.
 * 
 * @author akutz
 * 
 */
public class LdapAuthenticationService
{
    private static final Logger log = Logger
        .getLogger(LdapAuthenticationService.class);

    private static final String USE_SSL_PROP =
        "com.lostcreations.jira.seraph.ldapbind.useSsl";
    private static final String IGNORE_SSL_PROP =
        "com.lostcreations.jira.seraph.ldapbind.ignoreSslWarnings";

    /**
     * Authenticates a user against an Active Directory using a direct bind.
     * 
     * @param domain The domain.
     * @param userName The user name to on whose behalf the authentication is
     *        being performed.
     * @param ldapUserName The user name to use as part of the authentication
     *        credentials if different than the current user name.
     * @param password The password.
     * @return A flag indicating whether or not the authentication attempt was
     *         successful.
     * @throws Exception When an authenticator-related exception occurs.
     */
    public boolean authenticate(
        String domain,
        String userName,
        String ldapUserName,
        String password) throws Exception
    {
        checkNotNull(domain);
        checkNotNull(userName);
        checkNotNull(password);

        if (Strings.isNullOrEmpty(password))
        {
            error("empty password login attempt f/ '%s@%s'", userName, domain);
            return false;
        }

        if (ldapUserName == null) ldapUserName = userName;
        debug("authenticating '%s' as '%s'", userName, ldapUserName);

        Properties props = System.getProperties();
        boolean useSsl = props.containsKey(USE_SSL_PROP);
        boolean ignoreSsl = props.containsKey(IGNORE_SSL_PROP);

        String scheme;
        int port;
        if (useSsl)
        {
            scheme = "ldaps";
            port = 636;
        }
        else
        {
            scheme = "ldap";
            port = 389;
        }

        String bindUser = String.format("%s@%s", ldapUserName, domain);
        String url = String.format("%s://%s:%s/", scheme, domain, port);

        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(
            Context.INITIAL_CONTEXT_FACTORY,
            "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, bindUser);
        env.put(Context.SECURITY_CREDENTIALS, password);
        if (useSsl)
        {
            env.put(Context.SECURITY_PROTOCOL, "ssl");
        }
        if (ignoreSsl)
        {
            env.put("java.naming.ldap.factory.socket", SslSocketFactoryEx.class
                .getName());
        }

        LdapContext ldapCtx = null;

        try
        {
            debug("binding w/ '%s' to '%s'", bindUser, url);
            ldapCtx = new InitialLdapContext(env, null);
            info("successfully authenticated '%s' as '%s'", userName, bindUser);
            return true;
        }
        catch (AuthenticationException e)
        {
            error("error authenticating '%s' as '%s'", userName, bindUser);
            return false;
        }
        catch (NamingException e)
        {
            String errmsg =
                String.format("error binding '%s' as '%s'", userName, bindUser);
            error(errmsg, e);
            throw new Exception(errmsg);
        }
        finally
        {
            close(ldapCtx);
        }
    }

    private void close(DirContext context)
    {
        if (context == null) return;

        try
        {
            context.close();
            debug("closed context");
        }
        catch (Exception e)
        {
            error("error closing context", e);
        }
    }

    private void info(String format, Object... args)
    {
        LogUtil.log(log, Level.INFO, format, args);
    }

    private void debug(String format, Object... args)
    {
        LogUtil.log(log, Level.DEBUG, format, args);
    }

    private void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }
}
