package com.lostcreations.atlassian.seraph.ldapbind;

import static com.google.common.base.Preconditions.*;
import java.security.Principal;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * The service that provides the authentication-related methods to the Seraph
 * authenticator and Stash HTTP handler.
 * 
 * @author akutz
 * 
 */
public class AuthenticationManagerService
{
    private static final Logger log = Logger
        .getLogger(AuthenticationManagerService.class);

    /**
     * The user property that indicates to which domain they should
     * authenticate. This should be in the form of a fully-qualified domain
     * name, such as <code>ad.mycompany.com</code>.
     */
    public static final String PROP_DOMAIN = "seraph ldapbind domain";

    /**
     * The user name of the AD user with which the authenticating user should
     * perform the direct bind. This is useful if the Atlassian's user's user
     * name is different than that of their user name in Active Directory.
     */
    public static final String PROP_ADUSER = "seraph ldapbind aduser";

    /**
     * The service used to build user principals.
     */
    private final LdapUserService ldapUserService;

    /**
     * The service used to authenticate LDAP users.
     */
    private final LdapAuthenticationService ldapAuthSvc;

    /**
     * The service used to authenticate local users.
     */
    private final LocalAuthenticationService localAuthSvc;

    /**
     * The service used to query the local database to see if users do in fact
     * exist.
     */
    private final UserQueryService userQuerySvc;

    private final Object userService;

    public AuthenticationManagerService()
    {
        this(null);
    }

    public AuthenticationManagerService(Object userService)
    {
        this.userService = userService;

        this.userQuerySvc = new UserQueryService(this.userService);
        this.ldapUserService = new LdapUserService(this.userQuerySvc);
        this.ldapAuthSvc = new LdapAuthenticationService();
        this.localAuthSvc = new LocalAuthenticationService(this.userService);
    }

    public Principal getUser(String userName)
    {
        try
        {
            return getUserInner(userName);
        }
        catch (Exception e)
        {
            error("error getting user f/ '%s'", userName, e);
            return null;
        }
    }

    private Principal getUserInner(String userName)
    {
        checkNotNull(userName);
        return this.userQuerySvc.getUserByName(userName);
    }

    public boolean authenticate(Principal userPrincipal, String password)
        throws Exception
    {
        try
        {
            return authenticateInner(userPrincipal, password);
        }
        catch (Exception e)
        {
            String userName = "";

            try
            {
                if (userPrincipal != null) userName = userPrincipal.getName();
            }
            catch (Exception e1)
            {
                error("error getting username from principal", e);
                throw e;
            }

            error("error authenticating '%s'", userName, e);
            throw e;
        }
    }

    private boolean authenticateInner(Principal userPrincipal, String password)
        throws Exception
    {
        checkNotNull(userPrincipal);
        checkNotNull(password);

        String userName = userPrincipal.getName();
        debug("authenticating '%s'", userName);

        // Switch to an LDAP principal if available.
        LdapUser ldapUser = this.ldapUserService.getLdapUser(userName);

        boolean result;

        if (ldapUser == null)
        {
            debug("doing local authentication f/ '%s'", userName);
            result = this.localAuthSvc.authenticate(userPrincipal, password);

            if (result)
            {
                info("locally authenticated '%s'", userPrincipal.getName());
            }
        }
        else
        {
            debug("doing ldap authentication f/ '%s' as '%s'", ldapUser
                .getUserName(), ldapUser);

            if (ldapUser.isTrusted())
            {
                warn("'%s' is trusted", ldapUser);
                return true;
            }

            result =
                this.ldapAuthSvc.authenticate(ldapUser.getDomain(), ldapUser
                    .getUserName(), ldapUser.getBindUserName(), password);

            if (result)
            {
                info("ldap authenticated '%s'", ldapUser);
            }
        }

        if (!result)
        {
            error("error authenticating '%s'", userPrincipal.getName());
        }

        return result;
    }

    private void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }

    private void info(String format, Object... args)
    {
        LogUtil.log(log, Level.WARN, format, args);
    }

    private void warn(String format, Object... args)
    {
        LogUtil.log(log, Level.WARN, format, args);
    }

    private void debug(String format, Object... args)
    {
        LogUtil.log(log, Level.DEBUG, format, args);
    }

}
