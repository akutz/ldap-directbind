package com.lostcreations.atlassian.seraph.ldapbind;

import java.security.Principal;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import com.atlassian.stash.auth.HttpAuthenticationContext;
import com.atlassian.stash.auth.HttpAuthenticationHandler;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.user.IncorrectPasswordAuthenticationException;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;

public class LdapHttpAuthenticationHandler implements HttpAuthenticationHandler
{
    private static final Logger log = Logger
        .getLogger(LdapHttpAuthenticationHandler.class);

    private final AuthenticationManagerService authMgrService;
    private static final Object[] EMPTY_OBJ_ARR = new Object[0];
    private final I18nService i18nService;

    public LdapHttpAuthenticationHandler(
        I18nService i18nService,
        UserService userService)
    {
        this.i18nService = i18nService;
        this.authMgrService = new AuthenticationManagerService(userService);
    }

    @Override
    public StashUser authenticate(HttpAuthenticationContext authContext)
    {
        String userName = authContext.getUsername();
        if (userName == null)
        {
            debug("userName is null");
            return null;
        }

        Object pwd = authContext.getCredentials();
        if (pwd == null)
        {
            error("password is null f/ user='%s'", userName);
            return newBadCredentials();
        }

        if (!(pwd instanceof String))
        {
            error("password is not String f/ user='%s'", userName);
            return newBadCredentials();
        }

        String password = (String) pwd;
        Principal p = null;

        try
        {
            p = this.authMgrService.getUser(userName);
        }
        catch (Exception e)
        {
            error("error getting user='%s'", userName, e);
            return newBadCredentials();
        }

        if (p == null)
        {
            error("user principal is null for '%s'", userName);
            return newBadCredentials();
        }

        try
        {
            if (this.authMgrService.authenticate(p, password))
            {
                if (p instanceof StashUser)
                {
                    return (StashUser) p;
                }
                else
                {
                    error("userPrincipal='%s' != StashUser, ='%s'", userName, p
                        .getClass()
                        .getName());
                    return newBadCredentials();
                }
            }
            else
            {
                error("error authenticating user='%s'", userName);
                return newBadCredentials();
            }
        }
        catch (Exception e)
        {
            error("error authenticating user='%s'", userName, e);
            return newBadCredentials();
        }
    }

    @Override
    public void validateAuthentication(HttpAuthenticationContext authContext)
    {
        // Do nothing
    }

    private StashUser newBadCredentials()
    {
        throw new IncorrectPasswordAuthenticationException(this.i18nService
            .getKeyedText(
                "stash.auth.failed",
                "Invalid username or password.",
                EMPTY_OBJ_ARR));
    }

    private void debug(String format, Object... args)
    {
        LogUtil.log(log, Level.DEBUG, format, args);
    }

    private void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }
}
