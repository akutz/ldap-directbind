package com.lostcreations.atlassian.seraph.ldapbind;

import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

public class FakeTrustManager implements X509TrustManager
{
    @Override
    public void checkClientTrusted(X509Certificate[] xcs, String string)
    {
        // Do nothing
    }

    @Override
    public void checkServerTrusted(X509Certificate[] xcs, String string)
    {
        // Do nothing
    }

    @Override
    public X509Certificate[] getAcceptedIssuers()
    {
        return new java.security.cert.X509Certificate[0];
    }
}
