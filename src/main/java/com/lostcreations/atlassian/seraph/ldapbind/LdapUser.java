package com.lostcreations.atlassian.seraph.ldapbind;

import static com.google.common.base.Preconditions.*;
import java.security.Principal;

/**
 * An LDAP-enhanced user principal that wraps an Atlassian Crown principal
 * representing the same user.
 * 
 * @author akutz
 * 
 */
public class LdapUser
{
    private final Principal user;
    private final String domain;
    private final String bindUserName;
    private final String toString;
    private final boolean trusted;

    /**
     * Initializes a new instance of the {@link LdapUser} class.
     * 
     * @param user The underlying security principal.
     * @param domain The domain to which the user authenticates.
     * @param bindUserName The user name used in the Active Directory bind
     *        attempt.
     * @param trusted A flag indicating whether the user's password requires
     *        verification.
     */
    public LdapUser(
        Principal user,
        String domain,
        String bindUserName,
        boolean trusted)
    {
        checkNotNull(user);
        checkNotNull(domain);

        this.user = user;
        this.domain = domain;
        this.bindUserName = bindUserName;
        this.trusted = trusted;
        this.toString = String.format("%s@%s", this.bindUserName, this.domain);
    }

    public String getDomain()
    {
        return this.domain;
    }

    public String getBindUserName()
    {
        return this.bindUserName;
    }

    public String getUserName()
    {
        return this.user.getName();
    }

    public Principal getUser()
    {
        return this.user;
    }

    public boolean isTrusted()
    {
        return trusted;
    }

    @Override
    public String toString()
    {
        return this.toString;
    }
}
