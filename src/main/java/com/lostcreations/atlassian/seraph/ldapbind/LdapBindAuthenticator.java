package com.lostcreations.atlassian.seraph.ldapbind;

import java.security.Principal;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import com.atlassian.seraph.auth.AuthenticationContextAwareAuthenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.auth.DefaultAuthenticator;

/**
 * A Seraph authenticator that validates credentials by directly binding to an
 * active directory domain instead of using a look-up account.
 * 
 * @author akutz
 * 
 */
@AuthenticationContextAwareAuthenticator
public class LdapBindAuthenticator extends DefaultAuthenticator
{
    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 2596489253082843296L;

    private static final Logger log = Logger
        .getLogger(LdapBindAuthenticator.class);

    private final AuthenticationManagerService authMgrService;

    public LdapBindAuthenticator()
    {
        this.authMgrService = new AuthenticationManagerService();
    }

    @Override
    protected Principal getUser(String userName)
    {
        return this.authMgrService.getUser(userName);
    }

    @Override
    protected boolean authenticate(Principal userPrincipal, String password)
        throws AuthenticatorException
    {
        try
        {
            return this.authMgrService.authenticate(userPrincipal, password);
        }
        catch (Exception e)
        {
            error("error authenticating user='%s'", userPrincipal.getName(), e);
            throw new AuthenticatorException(e.getMessage());
        }
    }

    private void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }
}
