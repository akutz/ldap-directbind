package com.lostcreations.atlassian.seraph.ldapbind;

import static com.google.common.base.Preconditions.*;
import static com.lostcreations.atlassian.seraph.ldapbind.PropertiesService.*;
import java.security.Principal;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * A service for authenticating principals against the local store.
 * 
 * @author akutz
 * 
 */
public class LocalAuthenticationService
{
    private static final Logger log = Logger
        .getLogger(LocalAuthenticationService.class);

    private final Object stashUserService;

    public LocalAuthenticationService(Object stashUserService)
    {
        this.stashUserService = stashUserService;
    }

    /**
     * Authenticates a user.
     * 
     * @param principal The user principal to authenticate.
     * @param password The principal's credentials.
     * @return True if successful; otherwise false.
     * @throws Exception When an error occurs.
     */
    public boolean authenticate(Principal userPrincipal, String password)
        throws Exception
    {
        checkNotNull(userPrincipal);
        checkNotNull(password);

        String userName = userPrincipal.getName();
        debug("authenticating '%s' w/ local auth", userName);

        try
        {
            boolean result;

            if (IS_JIRA)
            {
                result =
                    com.atlassian.jira.component.ComponentAccessor
                        .getCrowdService()
                        .authenticate(userName, password) != null;
            }
            else if (IS_CONFLUENCE)
            {
                result =
                    ((com.atlassian.confluence.user.UserAccessor) com.atlassian.spring.container.ContainerManager
                        .getComponent("userAccessor")).authenticate(
                        userName,
                        password);
            }
            else if (IS_STASH)
            {
                result =
                    ((com.atlassian.stash.user.UserService) this.stashUserService)
                        .authenticate(userName, password) != null;
            }
            else
            {
                result = false;
            }

            if (result)
            {
                info("successfully authenticated '%s' w/ local auth", userName);
                return true;
            }
            else
            {
                debug("error authenticated w/ user '%s'", userName);
                return false;
            }
        }
        catch (Exception e)
        {
            String eclassName = e.getClass().getSimpleName();

            if (eclassName.equals("AccountNotFoundException"))
            {
                warn("error authenticating w/ unknown account '%s'", userName);
            }
            else if (eclassName.equals("FailedAuthenticationException"))
            {
                warn("error authenticating w/ bad user/pass '%s'", userName);
                return false;
            }
            else if (eclassName.equals("CommunicationException"))
            {
                warn("error authenticating w/ bad user/pass '%s'", userName);
                throw new Exception(String.format(
                    "communications error authenticating '%s'",
                    userName), e);
            }
            else if (eclassName.equals("OperationFailedException"))
            {
                error("uknown error authenticating '%s'", userName, e);
                throw new Exception(String.format(
                    "uknown error authenticating '%s'",
                    userName), e);
            }
            else
            {
                error("errror authenticating '%s'", userName, e);
                throw e;
            }
        }

        return false;
    }

    private void info(String format, Object... args)
    {
        LogUtil.log(log, Level.INFO, format, args);
    }

    private void debug(String format, Object... args)
    {
        LogUtil.log(log, Level.DEBUG, format, args);
    }

    private void warn(String format, Object... args)
    {
        LogUtil.log(log, Level.WARN, format, args);
    }

    private void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }
}
