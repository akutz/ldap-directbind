package com.lostcreations.atlassian.seraph.ldapbind;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * A service for accessing the authenticator's properties.
 * 
 * @author akutz
 * 
 */
public class PropertiesService
{
    private static final Logger log;

    /**
     * A flag that is true if the current application is JIRA.
     */
    public static final boolean IS_JIRA;

    /**
     * A flag that is true if the current application is Confluence.
     */
    public static final boolean IS_CONFLUENCE;

    /**
     * A flag that is true if the current application is Stash.
     */
    public static final boolean IS_STASH;

    public static final String LDAPBIND_PROP_FILE_NAME =
        "seraph-ldapbind.properties";

    public static final String USE_SSL_PROP = "useSsl";
    public static final String IGNORE_SSL_PROP = "ignoreSslWarnings";

    private final static String CAT_BASE = "catalina.base";

    private final static String JIRA_MARKER = "jira";
    private final static String JIRA_HOME = "jira.home";
    private final static String JIRA_PROPS = "jira-application.properties";

    private final static String CONF_MARKER = "confluence";
    private final static String CONF_HOME = "confluence.home";
    private final static String CONF_PROPS = "confluence-init.properties";

    private final static String STSH_MARKER = "stash";
    private final static String STSH_HOME = "stash.home";
    private final static String STSH_PROPS = "stash-application.properties";

    private final static File PROPS_FILE;
    private final static Properties DEFAULTS;

    static
    {
        log = Logger.getLogger(PropertiesService.class);

        String catBase = System.getProperty(CAT_BASE);
        if (catBase == null)
        {
            PROPS_FILE = null;
            IS_JIRA = false;
            IS_CONFLUENCE = false;
            IS_STASH = false;
        }
        else
        {
            if (catBase.contains(JIRA_MARKER))
            {
                IS_JIRA = true;
                IS_CONFLUENCE = false;
                IS_STASH = false;
            }
            else if (catBase.contains(CONF_MARKER))
            {
                IS_JIRA = false;
                IS_CONFLUENCE = true;
                IS_STASH = false;
            }
            else if (catBase.contains(STSH_MARKER))
            {
                IS_JIRA = false;
                IS_CONFLUENCE = false;
                IS_STASH = true;
            }
            else
            {
                IS_JIRA = false;
                IS_CONFLUENCE = false;
                IS_STASH = false;
            }

            PROPS_FILE = getPropsFile();
        }

        DEFAULTS = new Properties();
        try
        {
            DEFAULTS.load(PropertiesService.class.getResourceAsStream("/"
                + LDAPBIND_PROP_FILE_NAME));
        }
        catch (IOException e)
        {
            error("error loading default properties", e);
        }
    };

    static void reloadDefaults(InputStream defaults)
    {
        DEFAULTS.clear();
        try
        {
            DEFAULTS.load(defaults);
        }
        catch (IOException e)
        {
            error("error updating defaults", e);
        }
    }

    public boolean useSsl()
    {
        return getProps().containsKey(USE_SSL_PROP);
    }

    public boolean ignoreSslProp()
    {
        return getProps().containsKey(IGNORE_SSL_PROP);
    }

    private static File getPropsFile()
    {
        File appData = null;

        if (IS_JIRA)
        {
            InputStream ins =
                PropertiesService.class.getResourceAsStream("/" + JIRA_PROPS);

            try
            {
                Properties appProps = new Properties();
                appProps.load(ins);
                appData = new File(appProps.getProperty(JIRA_HOME));
            }
            catch (IOException e)
            {
                error("error loading jira properties", e);
            }
        }
        else if (IS_CONFLUENCE)
        {
            InputStream ins =
                PropertiesService.class.getResourceAsStream("/" + CONF_PROPS);

            try
            {
                Properties appProps = new Properties();
                appProps.load(ins);
                appData = new File(appProps.getProperty(CONF_HOME));
            }
            catch (IOException e)
            {
                error("error loading confluence properies", e);
            }
        }
        else if (IS_STASH)
        {
            if (System.getenv().containsKey("STASH_HOME"))
            {
                appData = new File(System.getenv("STASH_HOME"));
            }
            else
            {
                InputStream ins =
                    PropertiesService.class.getResourceAsStream("/"
                        + STSH_PROPS);
                try
                {
                    Properties appProps = new Properties();
                    appProps.load(ins);
                    appData = new File(appProps.getProperty(STSH_HOME));
                }
                catch (IOException e)
                {
                    error("error loading stash properies", e);
                }
            }
        }

        File propsFile = new File(appData, LDAPBIND_PROP_FILE_NAME);
        return propsFile;
    }

    /**
     * Gets the authenticator's properties.
     * 
     * @return The authenticator's properties.
     */
    public static Properties getProps()
    {
        Properties props = new Properties(DEFAULTS);
        if (PROPS_FILE == null || !PROPS_FILE.exists()) return props;

        try
        {
            props.load(new FileInputStream(PROPS_FILE));
        }
        catch (FileNotFoundException e)
        {
            error("error reading unknown file '%s'", PROPS_FILE, e);
        }
        catch (IOException e)
        {
            error("io exception reading props file '%s'", PROPS_FILE, e);
        }

        return props;
    }

    private static void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }
}
