package com.lostcreations.atlassian.seraph.ldapbind;

import static com.google.common.base.Preconditions.*;
import java.security.Principal;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * A service for parsing LDAP information.
 * 
 * @author akutz
 * 
 */
public class LdapUserService
{
    private static final Logger log = Logger.getLogger(LdapUserService.class);

    // The property keys for configured users.
    public static final String CONF_USER = "user";
    public static final String CONF_USER_KEY_ALLOWED = "allowed";
    public static final String CONF_USER_KEY_TRUSTED = "trusted";
    public static final String CONF_USER_KEY_DOMAIN = "domain";
    public static final String CONF_USER_KEY_BIND_USER = "bind.user";

    /**
     * The pattern used to match a user name such as akutz@mycompany.com.
     */
    private static final Pattern LOGIN_AT_SYNTAX = Pattern
        .compile("([\\w\\d.-]+)+@([\\w\\d.-]+)");

    /**
     * The pattern used to match a user name such as mycompany\akutz.
     */
    private static final Pattern LOGIN_NT_SYNTAX = Pattern
        .compile("([\\w\\d.-]+)[\\\\/]([\\w\\d.-]+)");

    /**
     * The service used to query the local database to see if users do in fact
     * exist.
     */
    private final UserQueryService userQuerySvc;

    /**
     * Initializes a new instance of the {@link LdapUserService} class.
     */
    public LdapUserService(UserQueryService userQuerySvc)
    {
        this.userQuerySvc = userQuerySvc;
    }

    /**
     * Gets an LDAP user for the given login information if possible, otherwise
     * returns null.
     * 
     * @param loginInfo The login information string from which the domain and
     *        user name are parsed.
     * @return A new {@link LdapUser} if the login information contains a valid
     *         domain and user name, otherwise null.
     */
    public LdapUser getLdapUser(String loginInfo)
    {
        checkNotNull(loginInfo);

        // Parse the login information.
        String[] loginInfoParts = parseLoginInfo(loginInfo);
        String domain = loginInfoParts[0];
        String userName = loginInfoParts[1];

        Principal userPrincipal = this.userQuerySvc.getUserByName(userName);
        if (userPrincipal == null)
        {
            error("invalid user name '%s'", userName);
            return null;
        }

        LdapUser ldapUser = getLdapUser(userPrincipal, domain);
        if (ldapUser == null) return null;

        debug("created ldap user '%s'", ldapUser);
        return ldapUser;
    }

    /**
     * Parses the login information into an array of two elements: the domain
     * and the user name.
     * 
     * @param loginInfo The login information.
     * @return An array with two elements: the domain and the user name. The
     *         domain may be null, but the user name will be set to the value of
     *         the login information if the parse failed.
     */
    String[] parseLoginInfo(String loginInfo)
    {
        Matcher atm = LOGIN_AT_SYNTAX.matcher(loginInfo);
        Matcher ntm = LOGIN_NT_SYNTAX.matcher(loginInfo);

        String domain = null;
        String userName = loginInfo;

        if (atm.find())
        {
            debug("processing '%s' as at-pattern ldap login", loginInfo);
            userName = atm.group(1);
            domain = atm.group(2);
        }
        else if (ntm.find())
        {
            debug("processing '%s' as nt-pattern ldap login", loginInfo);
            domain = ntm.group(1);
            userName = ntm.group(2);
        }
        else if (log.isDebugEnabled())
        {
            debug("'%s' does not contain ldap login information", loginInfo);
        }

        return new String[]
        {
            domain, userName
        };
    }

    private LdapUser getLdapUser(Principal userPrincipal, String domain)
    {
        checkNotNull(userPrincipal);

        String userName = userPrincipal.getName();
        Properties props = PropertiesService.getProps();
        userName = userName.toLowerCase();
        if (domain != null) domain = domain.toLowerCase();

        if (!isAllowed(props, domain, userName))
        {
            debug("LDAP disallowed for '%s\\%s'; restricted", domain, userName);
            return null;
        }

        String domainVal = getDomain(props, domain, userName);
        if (domainVal == null)
        {
            debug("LDAP disallowed for '%s\\%s'; no domain", domain, userName);
            return null;
        }

        String bindUser = getBindUser(props, domain, userName);
        boolean trusted = isTrusted(props, domain, userName);

        LdapUser ldapUser =
            new LdapUser(userPrincipal, domainVal, bindUser, trusted);
        debug(
            "LDAP allowed for '%s\\%s[%s]', trusted='%s'",
            domain,
            userName,
            bindUser,
            trusted);
        return ldapUser;
    }

    /**
     * Gets a property using the given domain and user.
     * 
     * @param props The properties.
     * @param domain The domain scope.
     * @param user The user scope.
     * @param key The property's key.
     * @param defaultVal The property's default value.
     * @return The property if it exists; otherwise null.
     */
    private String getProperty(
        Properties props,
        String domain,
        String user,
        String key,
        String defaultVal)
    {
        String root = CONF_USER;
        String keyPath;
        String val = null;

        keyPath =
            String.format("%s.domain.%s.user.%s.%s", root, domain, user, key);
        val = props.getProperty(keyPath, defaultVal);
        if (val != null)
        {
            debug("got domain\\user property '%s'='%s'", keyPath, val);
            return val;
        }

        keyPath = String.format("%s.domain.%s.%s", root, domain, key);
        val = props.getProperty(keyPath, defaultVal);
        if (val != null)
        {
            debug("got domain property '%s'='%s'", keyPath, val);
            return val;
        }

        keyPath = String.format("%s.%s.%s", root, user, key);
        val = props.getProperty(keyPath, defaultVal);
        if (val != null)
        {
            debug("got user property '%s'='%s'", keyPath, val);
            return val;
        }

        keyPath = String.format("%s.%s", root, key);
        val = props.getProperty(keyPath, defaultVal);
        debug("got root property '%s'='%s'", keyPath, val);
        return val;
    }

    private String getProperty(
        Properties props,
        String domain,
        String user,
        String key)
    {
        return getProperty(props, domain, user, key, null);
    }

    private boolean getBool(
        Properties props,
        String domain,
        String user,
        String key)
    {
        return Boolean.parseBoolean(getProperty(props, domain, user, key));
    }

    private boolean isAllowed(Properties props, String domain, String user)
    {
        return getBool(props, domain, user, CONF_USER_KEY_ALLOWED);
    }

    private boolean isTrusted(Properties props, String domain, String user)
    {
        return getBool(props, domain, user, CONF_USER_KEY_TRUSTED);
    }

    private String getDomain(Properties props, String domain, String user)
    {
        String val = getProperty(props, domain, user, CONF_USER_KEY_DOMAIN);

        if (domain != null && domain.contains("."))
            return val == null ? domain : val;

        return val;
    }

    private String getBindUser(Properties props, String domain, String user)
    {
        String val = getProperty(props, domain, user, CONF_USER_KEY_BIND_USER);
        return val == null ? user : val;
    }

    private void debug(String format, Object... args)
    {
        LogUtil.log(log, Level.DEBUG, format, args);
    }

    private void error(String format, Object... args)
    {
        LogUtil.log(log, Level.ERROR, format, args);
    }
}
