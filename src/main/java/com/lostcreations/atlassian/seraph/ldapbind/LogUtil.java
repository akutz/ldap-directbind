package com.lostcreations.atlassian.seraph.ldapbind;

import java.util.Arrays;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * A log utility.
 * 
 * @author akutz
 * 
 */
public final class LogUtil
{
    /**
     * Do not allow this class to be instantiated.
     */
    private LogUtil()
    {
    }

    /**
     * Formats and then emits a message to the given logger.
     * 
     * @param logger The logger to which to emit the message.
     * @param level The log level at which to emit the message.
     * @param format The message format.
     * @param args The message format's arguments. The last argument, if a
     *        Throwable, will be treated specially.
     */
    public static void log(
        Logger logger,
        Level level,
        String format,
        Object... args)
    {
        if (args == null || args.length == 0)
        {
            logger.log(level, format);
        }
        else if (args.length == 1 && args[0] instanceof Throwable)
        {
            logger.log(level, format, (Throwable) args[0]);
        }
        else if (!(args[args.length - 1] instanceof Throwable))
        {
            logger.log(level, String.format(format, args));
        }
        else
        {
            Throwable t = (Throwable) args[args.length - 1];
            Object[] argsLessOne = Arrays.copyOf(args, args.length - 1);
            logger.log(level, String.format(format, argsLessOne), t);
        }
    }

    /**
     * Formats and then emits a message to the given logger.
     * 
     * @param logger The logger to which to emit the message.
     * @param level The log level at which to emit the message.
     * @param format The message format.
     * @param args The message format's arguments. The last argument, if a
     *        Throwable, will be treated specially.
     */
    public static void log(
        org.slf4j.Logger logger,
        Level level,
        String format,
        Object... args)
    {
        if (args == null || args.length == 0)
        {
            logger.debug(format);
        }
        else if (args.length == 1 && args[0] instanceof Throwable)
        {
            logger.debug(format, (Throwable) args[0]);
        }
        else if (!(args[args.length - 1] instanceof Throwable))
        {
            logger.debug(String.format(format, args));
        }
        else
        {
            Throwable t = (Throwable) args[args.length - 1];
            Object[] argsLessOne = Arrays.copyOf(args, args.length - 1);
            logger.debug(String.format(format, argsLessOne), t);
        }
    }
}
