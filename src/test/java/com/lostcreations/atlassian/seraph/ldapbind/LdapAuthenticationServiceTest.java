package com.lostcreations.atlassian.seraph.ldapbind;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * The test class for {@link LdapAuthenticationService}.
 * 
 * @author akutz
 * 
 */
public class LdapAuthenticationServiceTest
{
    private final LdapAuthenticationService svc =
        new LdapAuthenticationService();

    @Test
    public void authenticateTest_ExpectedSuccess() throws Exception
    {
        String dom = System.getProperty("ldapbind.domain");
        String usr = System.getProperty("ldapbind.username");
        String pwd = System.getProperty("ldapbind.password");
        if (dom == null || usr == null || pwd == null) return;

        Assert.assertTrue(svc.authenticate(dom, usr, null, pwd));
        Assert.assertTrue(svc.authenticate(dom, usr, usr, pwd));
    }

    @Test
    public void authenticateTest_ExpectedFailure() throws Exception
    {
        String dom = System.getProperty("ldapbind.domain");
        String usr = System.getProperty("ldapbind.username");
        String pwd = System.getProperty("ldapbind.password");
        if (dom == null || usr == null || pwd == null) return;

        Assert.assertFalse(svc.authenticate(dom, usr, null, pwd + "@"));
        Assert.assertFalse(svc.authenticate(dom, usr, usr, pwd + "@"));
    }
}
