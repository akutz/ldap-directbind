package com.lostcreations.atlassian.seraph.ldapbind;

import java.io.InputStream;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * The test class for {@link LdapUserService}.
 * 
 * @author akutz
 * 
 */
public class LdapUserServiceTest
{
    private final LdapUserService svc = new LdapUserService(
        new UserQueryService(null));

    @Test
    public void getLdapUserPartsTest_AllowAll() throws Exception
    {
        Assert.assertNull(svc.getLdapUser("akutz"));
        Assert.assertNull(svc.getLdapUser("akutz@dev"));
        Assert.assertNotNull(svc.getLdapUser("akutz@dev.null"));
        Assert.assertNull(svc.getLdapUser("dev\\akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev.null\\akutz"));
        Assert.assertNull(svc.getLdapUser("dev/akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev.null/akutz"));
    }

    @Test
    public void getLdapUserPartsTest_DenyAll() throws Exception
    {
        PropertiesService
            .reloadDefaults(getProps("seraph-ldapbind-denyall.properties"));
        Assert.assertNull(svc.getLdapUser("akutz"));
        Assert.assertNull(svc.getLdapUser("akutz@dev"));
        Assert.assertNull(svc.getLdapUser("akutz@dev.null"));
        Assert.assertNull(svc.getLdapUser("dev\\akutz"));
        Assert.assertNull(svc.getLdapUser("dev.null\\akutz"));
        Assert.assertNull(svc.getLdapUser("dev/akutz"));
        Assert.assertNull(svc.getLdapUser("dev.null/akutz"));
    }

    @Test
    public void getLdapUserPartsTest_AlwaysAllUser() throws Exception
    {
        PropertiesService
            .reloadDefaults(getProps("seraph-ldapbind-allow-user.properties"));
        Assert.assertNull(svc.getLdapUser("akutz"));
        Assert.assertNotNull(svc.getLdapUser("fu.bar\\akutz"));
        Assert.assertNull(svc.getLdapUser("akutz@dev"));
        Assert.assertNotNull(svc.getLdapUser("akutz@dev.null"));
        Assert.assertNull(svc.getLdapUser("dev\\akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev.null\\akutz"));
        Assert.assertNull(svc.getLdapUser("dev/akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev.null/akutz"));
    }

    @Test
    public void getLdapUserPartsTest_AlwaysAllDomain() throws Exception
    {
        PropertiesService
            .reloadDefaults(getProps("seraph-ldapbind-allow-domain.properties"));
        Assert.assertNull(svc.getLdapUser("akutz"));
        Assert.assertNull(svc.getLdapUser("fu.bar\\akutz"));
        Assert.assertNotNull(svc.getLdapUser("akutz@dev"));
        Assert.assertNotNull(svc.getLdapUser("akutz@dev.null"));
        Assert.assertNotNull(svc.getLdapUser("dev\\akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev.null\\akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev/akutz"));
        Assert.assertNotNull(svc.getLdapUser("dev.null/akutz"));
    }

    @Test
    public void getLdapUserPartsTest_DenyAllDomain() throws Exception
    {
        PropertiesService
            .reloadDefaults(getProps("seraph-ldapbind-denyall-domain.properties"));
        Assert.assertNull(svc.getLdapUser("akutz"));
        Assert.assertNotNull(svc.getLdapUser("fu.bar\\akutz"));
        Assert.assertNull(svc.getLdapUser("akutz@dev"));
        Assert.assertNull(svc.getLdapUser("akutz@dev.null"));
        Assert.assertNull(svc.getLdapUser("dev\\akutz"));
        Assert.assertNull(svc.getLdapUser("dev.null\\akutz"));
        Assert.assertNull(svc.getLdapUser("dev/akutz"));
        Assert.assertNull(svc.getLdapUser("dev.null/akutz"));
    }

    @Test
    public void getLdapUserPartsTest_WildcardDomain() throws Exception
    {
        PropertiesService
            .reloadDefaults(getProps("seraph-ldapbind-wildcard-domain.properties"));
        Assert.assertNotNull(svc.getLdapUser("akutz"));
    }

    private InputStream getProps(String resName) throws Exception
    {
        return getClass().getResourceAsStream(resName);
    }

    @Test
    public void parseLoginInfoTest_AtSyntax()
    {
        assertLdap(svc.parseLoginInfo("akutz@mycompany.com"));
    }

    @Test
    public void parseLoginInfoTest_NtSyntax()
    {
        assertLdap(svc.parseLoginInfo("mycompany.com\\akutz"));
    }

    @Test
    public void parseLoginInfoTest_NoLdap()
    {
        asserNotLdap(svc.parseLoginInfo("akutz"));
    }

    private void assertLdap(String[] parts)
    {
        Assert.assertNotNull(parts);
        Assert.assertEquals(2, parts.length);
        Assert.assertEquals("mycompany.com", parts[0]);
        Assert.assertEquals("akutz", parts[1]);
    }

    private void asserNotLdap(String[] parts)
    {
        Assert.assertNotNull(parts);
        Assert.assertEquals(2, parts.length);
        Assert.assertEquals(null, parts[0]);
        Assert.assertEquals("akutz", parts[1]);
    }

    @Test
    public void test()
    {
        char[] a =
            "\u0007\\6\u0015\u0004'V`\u0018\u0001-W.\u0007\rnY%\r"
                .toCharArray();
        int i = a.length;
        label0:
        {
            int i0 = 0;
            int i1 = 0;
            label1:
            {
                if (i <= 1)
                {
                    i0 = i;
                    i1 = 0;
                    break label1;
                }
                if (i > 0)
                {
                    i0 = i;
                    i1 = 0;
                }
                else
                {
                    break label0;
                }
            }
            while (true)
            {
                int i2 = 0;
                int i3 = i0;
                int i4 = i1;
                int i5 = i1;
                while (true)
                {
                    int i6 = 0;
                    int i7 = a[i4];
                    switch (i5 % 5)
                    {
                        case 3 :
                        {
                            i6 = 116;
                            break;
                        }
                        case 2 :
                        {
                            i6 = 64;
                            break;
                        }
                        case 1 :
                        {
                            i6 = 50;
                            break;
                        }
                        case 0 :
                        {
                            i6 = 78;
                            break;
                        }
                        default :
                        {
                            i6 = 104;
                        }
                    }
                    int i8 = (char) (i7 ^ i6);
                    a[i4] = (char) i8;
                    i2 = i5 + 1;
                    if (i3 != 0)
                    {
                        break;
                    }
                    else
                    {
                        i3 = 0;
                        i4 = 0;
                        i5 = i2;
                    }
                }
                if (i3 > i2)
                {
                    i0 = i3;
                    i1 = i2;
                }
                else
                {
                    break;
                }
            }
        }
        String z = new String(a).intern();
        System.out.println(z);
    }
}
